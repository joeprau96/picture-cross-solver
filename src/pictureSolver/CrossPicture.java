package pictureSolver;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class CrossPicture {
	public static char[][] crossPicture;
	private static Rows[] rows;
	private static Rows[] columns;
	private final static char CROSS = 'X';
	private final static char UNFILLED = ' ';
	private final static char FILLED = 219;
	public static int size;
	
	public static void main(String[] args) throws FileNotFoundException {
		fillCross();
		System.out.println("Finding Answer...");
		System.out.println(backtrack(0));
	    printCross();

	}
	
	//inputs from file to initialize program
	public static void fillCross() throws FileNotFoundException {
		Scanner fr = new Scanner(new File("MovieCamera.txt"));
		size = fr.nextInt();
		rows = new Rows[size];
		columns = new Rows[size];
		crossPicture = new char[size][size];
		//fills rows
		for(int i = 0; i<size; i++) {
			int activeCount = fr.nextInt();
			int active[] = new int[activeCount];
			for(int j = 0; j<activeCount;j++) {
				active[j] = fr.nextInt();
			}
			rows[i] = new Rows(active);
		}
		//fills columns
		for(int i = 0; i<size; i++) {
			int activeCount = fr.nextInt();
			int active[] = new int[activeCount];
			for(int j = 0; j<activeCount;j++) {
				active[j] = fr.nextInt();
			}
			columns[i] = new Rows(active);
		}
		
	}
	
	//prints the square
	public static void printCross() {
		for(int i = 0; i<size; i++) {
			for(int j = 0; j<size; j++) {
				System.out.print("" + crossPicture[i][j] + "  ");
			}
			System.out.println(" ");
		}
		System.out.println("---------------");
	}
	
	//backtracking based solver for program
	public static boolean backtrack(int cord){
		
		//calculates x and y for convenience
		int x = xFromCord(cord);
		int y = yFromCord(cord);
		
		//if cord outside of square, complete
		//base case
	    if (cord >= size*size){ 
	        return true;
	    }
	 
	    //Fill current square with positive answer
	    crossPicture[y][x] = FILLED;
	     if (gridIsValid(cord) == true){ // Check for collisions
	        	if (backtrack(cord+1) == true){ // Move to next empty cell
	            return true; // Empty cells filled. Solution found. Abort.
	        }
	        
		}
	    //if failed, use cross instead
		crossPicture[y][x] = CROSS;
		if (gridIsValid(cord) == true){ // Check for collisions
			if (backtrack(cord+1) == true){ // Move to next empty cell
				return true; // Empty cells filled. Solution found. Abort.
		    }
		       
		}
		//otherwise solution is impossible with previous cells, reset to last checkpoint
	    crossPicture[y][x] = UNFILLED; // Empties cell
	    return false; //Solution not found. Backtrack.
	}
	
	
	private static boolean gridIsValid(int cord) {
		//checks both column and row of specific section
		int x = xFromCord(cord);
		int y = yFromCord(cord);
		return rows[y].checkRow(y, crossPicture, size) && columns[x].checkColumn(x, crossPicture, size);
	}
	private static int xFromCord(int cord) {
		return cord%size;
	}
	private static int yFromCord(int cord) {
		return cord/size;
	}
	private int cordFromXY(int x, int y) {
		return y*size+x;
	}
}
class Rows{
	public int values[];


	private final char CROSS = 'X';
	private final int UNFILLED = ' ';
	private final int FILLED = 219;
	
	public Rows(int[] array) {
		values=array;
	}
	//row checker
	public boolean checkRow(int index, char[][]crossPicture, int size) {
		int j=0;
		int active;
		int counter;
		//checks for each number
		for(active = 0; active<values.length && j<size; active++) {
			
			//if current square is unfilled, end.
			if (j<size && crossPicture[index][j] == UNFILLED) {
				break;
			}
			//set counter to 0
			counter = 0;
			//chunk of X to be skipped past/
			while(j<size && crossPicture[index][j]==CROSS ) j++;
			//string of filled items.
			while(j<size && crossPicture[index][j]==FILLED) {
				j++;
				counter++;
				if (counter > values[active]) return false; //if filled exceeds value, false
			}
			//if line is too small, end
			if(j<size && crossPicture[index][j]==CROSS && counter<values[active]) return false;
			if(j>=size && counter < values[active]) return false;
		}
		//if (j>=size && active<values.length) return false;
		//if we are done checking numbers, checks that no more squares have been filled
		while(j<size) {
			if(crossPicture[index][j]==FILLED) return false;
			j++;
		}
		//if all passes, return true.
		return true;
		
	}
	
	
	public boolean checkColumn(int index, char[][] crossPicture, int size) {
		int j=0;
		int active;
		int counter;
		//checks for each number
		for(active = 0; active<values.length && j<size; active++) {
			//if current square is unfilled, end.
			if (j<size && crossPicture[j][index] == UNFILLED) {
				break;
			}
			counter = 0;
			//chunk of X to be skipped past/
			while(j<size && crossPicture[j][index]==CROSS ) j++;
			//string of filled items.
			while(j<size && crossPicture[j][index]==FILLED) {
				j++;
				counter++;
				if (counter > values[active]) return false; //If line is too long for number, break
			}
			//if line is too short for number, break
			if(j<size && crossPicture[j][index]==CROSS && counter<values[active]) return false;
			//if we are out of space, but there are more numbers, break
			if(j>=size && counter < values[active]) return false;
		}
		while(j<size) {
			//if there are any remaining squares that disqualifies row, break
			if(crossPicture[j][index]==FILLED) return false;
			j++;
		}
		return true;
	}
	
	public int[] getValues() {
		return values;
	}
	public void setValues(int[] values) {

		this.values = values;
	}

}
